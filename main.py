from flask import Flask, request
import json
import db

app = Flask(__name__)

# wait JSON like:
#{
#"id":1,
#}
@app.route('/set_cookie', methods=['POST'])
def status():
    id_data = request.data.decode('UTF-8')
    json_object = json.loads(id_data)
    id = json_object['id']
    cookie = db.add_cookie(id)
    return cookie


@app.route('/get_id', methods=['GET'])
def get_id():
    data_cookie = request.data.decode('UTF-8')
    json_object = json.loads(data_cookie)
    cookie = json_object["value"]
    status = db.get_id_player(cookie)
    print(status)
    return status



if __name__ == '__main__':
    app.run(debug=True, port=6002)
    print("ddwefwef")