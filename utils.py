import random, string

def generate_cookie():
    cookie = random.choice(string.ascii_letters)
    for i in range(14):
        cookie += random.choice(string.ascii_letters)
    return cookie