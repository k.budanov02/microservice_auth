import psycopg2

import utils

conn = psycopg2.connect(dbname='knb_auth', user='postgres', host='localhost', port='5432')
cursor = conn.cursor()
conn.autocommit = True

def add_cookie(id):
    cookie = utils.generate_cookie()
    cursor.execute(f'INSERT INTO players_cookie  values ({id},\'{cookie}\');')
    return cookie


def get_id_player(cookie):
    cursor.execute(f'SELECT id FROM players_cookie WHERE cookie = \'{cookie}\';')
    result = cursor.fetchone()
    if result == None:
        return 'False'
    else:
        return str(result[0])
